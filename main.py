import os, sys
import logging
from datetime import datetime, timedelta, date
from google.cloud import bigquery
from google.cloud import pubsub_v1

def config_logging():
    """This function will configure the logging system
    of the whole function
    """
    
    # Initial configuration of logging system
    logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level=logging.INFO,
        stream=sys.stdout
    )

def sending_to_pubsub(string, label):
    """This function will upload any correct message
    to a Cloud PubSub topic
    """
    # Init logging system
    config_logging()

    # Creation of a PubSub publisher in GCP_PUBSUB_TOPIC
    GCP_PUBSUB_TOPIC = os.getenv("GCP_PUBSUB_TOPIC")
    publisher = pubsub_v1.PublisherClient()
    topic_name = GCP_PUBSUB_TOPIC
    logging.info("Creating a Cloud PubSub client to send a message to a topic.")

    # Publishing message with date label
    message = publisher.publish(topic_name, b'%s' % str.encode(string), date=label)
    message.result()
    logging.info("Message sent to %s PubSub topic" % topic_name)


def checking_billing(event, context):
    """ This function will query the Billing export in BigQuery to 
    retrieve the amount of money spent on last 48 hours. The idea
    is to check the progression as the cost is too low to use a 
    smaller time window """

    # Init logging system
    config_logging()

    # Vars from Google Cloud environments
    logging.info("Getting config values from environment variables")
    THRESHOLD = os.getenv("THRESHOLD")
    INTERVAL = os.getenv("INTERVAL")
    B_PROJECT = os.getenv("BILLING_PROJECT")
    B_DATASET = os.getenv("BILLING_DATASET")
    B_TABLE = os.getenv("BILLING_TABLE")


    # Create a start date based in INTERVAL in days 
    logging.info("Calculating deltas...")
    today = (date.today()).strftime("%Y.%m.%d")
    end = datetime.now()
    start = end - timedelta(days = int(INTERVAL))
    date_end = end.strftime("%Y-%m-%d %X")
    date_start = start.strftime("%Y-%m-%d %X")

    # Create a client to connect to BigQuery
    client = bigquery.Client()
    logging.info("Creating a BigQuery client to check Cloud cost in GCP.")

    # Archetype for create the query
    query = (
        """
        SELECT
            (SUM(CAST(cost * 1000000 AS int64))
                + SUM(IFNULL((SELECT SUM(CAST(c.amount * 1000000 as int64))
                                FROM UNNEST(credits) c), 0))) / 1000000
                AS total_exact
        FROM `%s.%s.%s`
        WHERE usage_start_time > "%s"
        AND
                usage_end_time < "%s" """
        ) % (B_PROJECT, B_DATASET, B_TABLE, date_start, date_end)
    logging.info("Architecting the query to launch in BigQuery")

    gcp_cost_df = client.query(query).to_dataframe()
    logging.info("Transform output to dataframe to handle it easier.")

    # Closing the connection
    client.close()
    logging.info("Closing BigQuery client.")


    message = "Tu gasto total es %s €." % gcp_cost_df.total_exact.item()
    logging.info(message)

    if gcp_cost_df.total_exact.item() >= float(THRESHOLD):
        sending_to_pubsub(message, today)
        logging.info("Excessive Cloud cost. Sending a warning message to PubSub")
